#include "client_http.hpp"
#include "server_http.hpp"
#include <future>

// Added for the json-example
#define BOOST_SPIRIT_THREADSAFE
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

// Added for the default_resource example
#include <algorithm>
#include <boost/filesystem.hpp>
#include <fstream>
#include <vector>
#ifdef HAVE_OPENSSL
#include "crypto.hpp"
#endif

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include <iterator>

using namespace std;
// Added for the json-example:
using namespace boost::property_tree;

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;

const std::size_t MAX_BUFFER_BYTES = 128 * 1024;

void send_directory_entries(const boost::filesystem::path &request_absolute_path,
                            const std::string &request_path,
                            const std::string &address,
                            unsigned short port,
                            const shared_ptr<HttpServer::Response> &response);

void send_file(const boost::filesystem::path &request_file_path,
               const SimpleWeb::CaseInsensitiveMultimap &request_header,
               const shared_ptr<HttpServer::Response> &response);

void read_and_send(size_t buffer_size,
                   const shared_ptr<HttpServer::Response> &response,
                   const shared_ptr<ifstream> &ifs,
                   size_t left);

int main(int argc, char *argv[]) {
  // HTTP-server at port 8080 using 1 thread
  // Unless you do more heavy non-threaded processing in the resources,
  // 1 thread is usually faster than several threads
  HttpServer server;
  server.config.address = "127.0.0.1";
  server.config.port = 8080;

  std::string current_path;
  if(argc > 1) {
    current_path = argv[1];
  }
  if(argc > 2) {
    server.config.port = std::stoi(argv[2]);
  }

  // Default GET-example. If no other matches, this anonymous function will be called.
  // Will respond with content in the web/-directory, and its subdirectories.
  server.default_resource["GET"] = [&server, current_path](const shared_ptr<HttpServer::Response> &response,
                                                           const shared_ptr<HttpServer::Request> &request) {
    try {
      const auto app_path = current_path.empty() ? boost::filesystem::absolute(boost::filesystem::current_path())
                                                 : current_path;
      const auto request_absolute_path = app_path / request->path;

      if(boost::filesystem::is_directory(request_absolute_path)) {
        send_directory_entries(request_absolute_path,
                               request->path,
                               server.config.address,
                               server.config.port,
                               response);
      }
      else if(boost::filesystem::is_regular_file(request_absolute_path)) {
        send_file(request_absolute_path,
                  request->header,
                  response);
      }
    }
    catch(const exception &e) {
      cerr << ("Could not open path " + request->path + ": " + e.what()) << endl;
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "Could not open path " + request->path + ": " + e.what());
    }
  };

  server.on_error = [](shared_ptr<HttpServer::Request> /*request*/, const SimpleWeb::error_code & /*ec*/) {
    // Handle errors here
    // Note that connection timeouts will also call this handle with ec set to SimpleWeb::errc::operation_canceled
  };

  // Start server and receive assigned port when server is listening for requests
  promise<unsigned short> server_port;
  thread server_thread([&server, &server_port]() {
    // Start server
    server.start([&server_port](unsigned short port) {
      server_port.set_value(port);
    });
  });
  cout << "Server listening on port " << server_port.get_future().get() << endl
       << endl;

  server_thread.join();
}

void send_directory_entries(const boost::filesystem::path &request_absolute_path,
                            const std::string &request_path,
                            const std::string &address,
                            unsigned short port,
                            const shared_ptr<HttpServer::Response> &response) {
  std::stringstream response_stream;
  response_stream << "<!DOCTYPE html>\n<html>\n<body>" << std::endl;
  const std::string &base_url = "http://" + address + ":" + std::to_string(port);
  for(auto &entry : boost::make_iterator_range(boost::filesystem::directory_iterator(request_absolute_path), {})) {
    const boost::filesystem::path path_entry(entry);
    response_stream << "<a href=" << base_url << (request_path / path_entry.filename()).string() << ">"
                    << path_entry.filename() << "</a></br>\n";
    std::cout << "dir=" << request_absolute_path
              << ", file=" << ((request_path) / path_entry.filename()) << std::endl;
  }
  response_stream << "</body>\n</html>" << std::endl;
  response->write(response_stream);
}

void send_file(const boost::filesystem::path &request_file_path,
               const SimpleWeb::CaseInsensitiveMultimap &request_header,
               const shared_ptr<HttpServer::Response> &response) {
  bool is_partial = false;
  ifstream::pos_type start_range = 0;
  ifstream::pos_type end_range = -1;
  const auto range_it = request_header.find("Range");
  if(range_it != std::end(request_header)) {
    // skip 'bytes='
    const auto range = range_it->second.substr(6);
    const auto sep = range.find('-');
    if(sep != std::string::npos) {
      is_partial = true;
      start_range = std::stoi(range.substr(0, sep));
      if(sep + 1 < range.size()) {
        end_range = std::stoi(range.substr(sep + 1, range.size() - sep));
      }
    }
  }

  SimpleWeb::CaseInsensitiveMultimap response_header;

  // Uncomment the following line to enable Cache-Control
  // response_header.emplace("Cache-Control", "max-age=86400");

  const auto ifs = make_shared<ifstream>();
  ifs->open(request_file_path.string(), ifstream::in | ios::binary | ios::ate);

  if(*ifs) {
    const auto file_length = ifs->tellg();
    ifs->seekg(start_range);

    if(end_range == -1 || end_range >= file_length) {
      end_range = file_length - 1L;
    }
    if(is_partial) {
      response_header.emplace("Content-Range",
                              "bytes " + std::to_string(start_range) + "-" + std::to_string(end_range) + "/" + std::to_string(file_length));
    }
    const auto content_length = is_partial ? (end_range - start_range + 1L) : static_cast<unsigned long>(file_length);
    response_header.emplace("Content-Length", to_string(content_length));
    const auto status = is_partial ? SimpleWeb::StatusCode::success_partial_content
                                   : SimpleWeb::StatusCode::success_ok;
    response->write(status, response_header);
    read_and_send(std::min(content_length, MAX_BUFFER_BYTES),
                  response,
                  ifs,
                  content_length);
  }
  else {
    throw invalid_argument("could not read file");
  }
}

void read_and_send(const size_t buffer_size,
                   const shared_ptr<HttpServer::Response> &response,
                   const shared_ptr<ifstream> &ifs,
                   size_t left) {
  vector<char> buffer(std::min(static_cast<size_t>(left), buffer_size)); // Safe when server is running on one thread
  const auto read_length = ifs->read(buffer.data(), static_cast<streamsize>(buffer.size())).gcount();
  if(read_length > 0) {
    assert(static_cast<long>(left) - read_length >= 0);
    left -= read_length;
    std::cout << "response read_length=" << read_length << ",left=" << left << ",buffer.size=" << buffer.size() << std::endl;
    response->write(buffer.data(), read_length);
    if(read_length >= static_cast<streamsize>(buffer.size()) && left > 0) {
      response->send([buffer_size, response, ifs, left](const SimpleWeb::error_code &ec) {
        if(!ec) {
          read_and_send(buffer_size, response, ifs, left);
        }
        else {
          cerr << "Connection interrupted" << endl;
        }
      });
    }
  }
}
